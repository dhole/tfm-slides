\documentclass[mathserif]{beamer}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage{amsmath}
\usepackage{tikz}
\usetikzlibrary{arrows,chains,matrix,positioning,scopes}

\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\usefonttheme[onlymath]{serif}

\mode<presentation> {
  \usetheme{Madrid}
  \setbeamercovered{transparent}
  \usecolortheme{rose}
  \useoutertheme{infolines}
}


%Information to be included in the title page:
\title[Lattice-based encryption] %optional
{Post-Quantum Cryptography}

\subtitle{Lattice-based encryption}

\author[Eduard Sanou] % (optional, for multiple authors)
{Eduard Sanou}

\institute[UPC] % (optional)
{
  Master in Innovation and Research in Informatics\\
  Advanced Computing\\
  Facultat d'Informàtica de Barcelona (FIB)\\
  Universitat Politècnica de Catalunya (UPC) - BarcelonaTech
}

\date[2016, July 8] % (optional)
{Master Thesis, 2016 July}

%\logo{\includegraphics[height=1.5cm]{lion-logo.png}}

\newcommand{\BB}{\mathbf{B}}
\newcommand{\A}{\mathbf{A}}
\newcommand{\Zq}{\mathbb{Z}_q}
\newcommand{\Zqn}{\Zq^n}
\newcommand{\RRn}{\mathbb{R}^n}
\newcommand{\La}{\mathcal{L}}
\newcommand{\Lasx}{\La_{\mathbf{s}, \mathcal{X}}}
\newcommand{\Asx}{A_{\mathbf{s}, \mathcal{X}}}
\newcommand{\qq}{\lfloor \frac{q}{2} \rceil}
\newcommand{\qqk}{\lfloor \frac{q}{2k} \rceil}
\newcommand{\LWE}{(\mathbf{a}, c = \langle \mathbf{a}, \mathbf{s}
\rangle + e) \in \Zqn \times \Zq}
\newcommand{\RLWE}{(a, b = a \cdot s + e \text{ mod } q) \in R_q \times R_q}
\newcommand{\cyclo}{\mathbb{Z}[x] / \langle x^n + 1 \rangle}
\newcommand{\cycloq}{\mathbb{Z}_q[x] / \langle x^n + 1 \rangle}
\newcommand{\Pa}{\mathcal{P}_{1/2}(\tilde{\BB})}
\newcommand{\Paa}{\mathcal{P}_{1/2}(\mathbf{B})}
\newcommand{\Bb}{\BB = \{\mathbf{b}_1, \dotsc, \mathbf{b}_k\}}
\newcommand{\Ldis}{\text{smp}}

\DeclareMathOperator*{\argmin}{arg\,min}

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{Aim of this thesis}

  \begin{itemize}
    \item
      Part of this work has been done to support an industrial PhD working at
      Scytl on e-voting systems.

    \item
      The aim of this thesis was to study the current state of post-quantum
      cryptography and evaluate the viability of adapting current e-voting
      schemes to post-quantum cryptography.

    \item
      We have focused on lattice-based cryptography (out of the several
      post-quantum cryptography proposals).
  \end{itemize}
\end{frame}

\subsection{Cryptography concepts}
\begin{frame}
  \frametitle{Cryptography concepts}
  \begin{block}{Symmetric encryption}
    The same key $sk$ is used for encryption and decryption.

    $c = Enc_{sk}(m)$, $m = Dec_{sk}(c)$.
  \end{block}

  ~

  \begin{block}{Asymmetric (or public-key) encryption}
    A public key $pk$ is used to encrypt, and a different secret key $sk$ to
    decrypt.

    $c = Enc_{pk}(m)$, $m = Dec_{sk}(c)$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Cryptography concepts}

  \begin{itemize}
    \item
      Public-key cryptographic schemes are built in a way such that breaking
      them implies solving hard problems.
    \item
      What do we mean by ``breaking'' an encryption scheme?  Intuitively:
      revealing some information about the plaintext.  But there's more stronger
      and precise concepts: IND-CPA, IND-CCA.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Semantic Security (IND-CPA)}

\begin{center}
\begin{tabular}{ c c c }
\vspace{-40pt} & & \\
 & & \\
  \includegraphics[width=0.8cm]{figs/cha_b.png}
  & &
  \includegraphics[width=0.8cm]{figs/att.png}
  \vspace{-7pt}
\\
\scriptsize Challenger & & \scriptsize Attacker \\
& & \\
generate $pk, sk$
&
$pk$
\vspace{-7pt}
& \\
&
\vspace{-5pt}
\begin{tikzpicture}
\begin{scope}[thick,->]
  \draw (-2,0)--(2,0);
\end{scope}
\end{tikzpicture}
& \\
&\vspace{10pt} \scriptsize (public key) & \\
&
$m_0, m_1$
\vspace{-7pt}
& choose $m_0, m_1$ \\
&
\vspace{-5pt}
\begin{tikzpicture}
\begin{scope}[thick,->]
  \draw (2,0)--(-2,0);
\end{scope}
\end{tikzpicture}
& \\
&\vspace{10pt} \scriptsize (challenge plaintexts) & \\
pick $b \in \{0, 1\}$ \\ at random &
$c = Enc_{pk}(m_b)$
\vspace{-7pt}
& \\
&
\vspace{-5pt}
\begin{tikzpicture}
\begin{scope}[thick,->]
  \draw (-2,0)--(2,0);
\end{scope}
\end{tikzpicture}
& \\
& \scriptsize (challenge ciphertext) &
$m_b = m_0$? \vspace{-3pt}\\ 
& & or \vspace{-3pt} \\
& & $m_b = m_1$? \\
\end{tabular}
\end{center}
\end{frame}

\subsection{Post-Quantum Cryptography}
\begin{frame}
\frametitle{Current public-key schemes and quantum computers}

\begin{itemize}
  \item
    The most currently used algorithms for public-key cryptography are RSA,
    Diffie-Hellman, elliptic curve cryptography and ElGamal.
    \begin{itemize}
      \item
        \alert{RSA} is based on the hardness of the \alert{factoring} problem.
      \item
        \alert{Diffie-Hellman}, \alert{elliptic curve} cryptography and
        \alert{ElGamal} are based on the hardness of solving the \alert{discrete
        logarithm} problem.
    \end{itemize}

  \item
    A quantum algorithm (Shor) can solve these problems efficiently (polynomial
    time).

  \item
    Efficient and large quantum computers may be built in 30 years.

  \item
    Public-key cryptography is essential to protect all our digital
    infrastructures.

  \item
    Are we doomed?
\end{itemize}
\end{frame}

\section{Lattices}

\begin{frame}
\frametitle{Lattice definition}

\begin{block}{Lattice}
A lattice is a set of points in an $n$-dimensional space with a periodic
structure.

\begin{equation*}
    \La(\mathbf{B}) =
    \left\{\sum_{i=1}^k z_i \mathbf{b_i} : z_i \in \mathbb{Z}\right\} =
    \left\{\mathbf{B}\mathbf{z}: \mathbf{z} \in \mathbb{Z}^n\right\}
    \label{lattice_def}
\end{equation*}
\end{block}

\begin{itemize}
  \item We denote the basis of the lattice as the matrix $\BB = (\mathbf{b_1}, \dotsc
\mathbf{b_k})$, i.e., the matrix whose columns are $\mathbf{b_1}, \dotsc
\mathbf{b_k}$.
  \item A lattice has many equivalent bases.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Lattice example}
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figs/lat1.png}
        \caption{$\mathbf{b}_1 = [6, 0], \mathbf{b}_2 = [1, 4]$}
        \label{fig:lat1}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figs/lat2.png}
        \caption{$\mathbf{b}_1 = [8, 8], \mathbf{b}_2 = [9, 12]$}
        \label{fig:q-lat1}
    \end{subfigure}
    \caption{A two dimensional lattice with two equivalent bases.}
    \label{fig:lat_q-ary1}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Norm}
Many lattice problems require the notion of distance between two points.  We use
the Euclidean norm for this purpose.

\begin{block}{Euclidean norm}
    Let $\mathbf{w} \in \mathbb{R}^n$.  The Euclidean norm is the
    function $\|.\|_2$ defined by
    \begin{equation*}
        \|\mathbf{w}\|_2 = \sqrt{\sum_{i=1}^n |\mathbf{w}_i|^2}
    \end{equation*}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Minima}
\begin{block}{Minima}
The minimum $\lambda_i(\La)$ of a lattice $\La$ is defined as the radius of the
smallest hypersphere centered at the origin, containing $i$ linearly independent
lattice vectors.
\end{block}
The minimum distance between two lattice points will be $\lambda_1(\La)$ which
is the length of a shortest nonzero lattice vector.
\end{frame}

\begin{frame}
\frametitle{Minima}
\begin{figure}[H]
  \includegraphics[width=0.55\textwidth]{figs/lambda.png}
  \caption{$\lambda_1(\La)$ in blue, $\lambda_2(\La)$ in red.}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Lattice problems}
\begin{itemize}
  \item Many lattice problems are hard in the average case.
    \begin{itemize}
      \item NP-Hard problems are usually only hard in the worst case.
    \end{itemize}
  \item Most of them exist in approximate and exact version.
    \begin{itemize}
      \item Up to a \alert{polynomial} factor of $n$ are \alert{hard}.
      \item At an \alert{exponential} factor of $n$ are \alert{easy}.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Lattice problems involving distances}
  \begin{block}{Closest Vector Problem (CVP)}
    Given a basis $\mathbf{B}$ of an $n$-dimensional lattice $\La(B)$
    and a target vector $\mathbf{t} \in \mathbb{R}^n$ (not necessarily in the
    lattice), find the closest lattice point $\mathbf{u} \in \La(\mathbf{B})$,
    i.e., find $\mathbf{u}$ such that $\mathbf{u} = \argmin\limits_{\mathbf{v}
    \in \La} \|\mathbf{t} - \mathbf{v}\|$.
\end{block}

\begin{block}{Bounded Distance Decoding Problem (BDD)}
    Given a basis $\mathbf{B}$ of an $n$-dimensional lattice $\La(B)$ and
    a target point $\mathbf{t} \in \mathbb{R}^n$ with the guarantee that
    $dist(\mathbf{t}, \La) < d = \lambda_1(\La)/(2 \gamma(n))$, find the unique
    lattice vector $\mathbf{v} \in \La$ such that $\|\mathbf{t} - \mathbf{v}\| <
    d$.
\end{block}

\end{frame}

\begin{frame}
  \frametitle{Bounded Distance Decoding (BDD)}
\begin{figure}[H]
  \includegraphics[width=0.55\textwidth]{figs/bdd.png}
  \caption{Example where $dist(\mathbf{t}, \La) < d = \lambda_1(\La)/2 $, i.e.,
  $\gamma(n) = 1$.}
\end{figure}
\end{frame}

\section{Learning With Errors (LWE)}

\begin{frame}
\frametitle{Learning With Errors definition}
\begin{block}{Learning With Errors (LWE)}
    Let $n$, $q$ be positive integers, $\mathcal{X}$ be a discrete probability
    distribution on $\mathbb{Z}$ called the error distribution and $\mathbf{s}$
    a secret vector in $\Zqn$.  We denote by $\Lasx$ the probability
    distribution on $\Zqn \times \Zq$ obtained by choosing $\mathbf{a} \in \Zqn$
    uniformly at random, choosing $e \in \mathbb{Z}$ according to $\mathcal{X}$
    and considering it in $\Zq$, and returning $\LWE$.
\end{block}

\end{frame}

\begin{frame}
\frametitle{Learning With Errors definition}
  Two versions of the problem:
\begin{itemize}
  \item
    \alert{Decision-LWE}: deciding whether pairs $(\mathbf{a},
    c) \in \Zqn \times \Zq$ are sampled according to $\Lasx$ or the uniform
    distribution on $\Zqn \times \Zq$.

  \item
    \alert{Search-LWE}: recovering $\mathbf{s}$ from
    $\LWE$ sampled according to $\Lasx$.
\end{itemize}
~

In each version we are always given a polynomial number of samples $m$ with the
same secret $s$.

~

Since Decision-LWE is hard, we can treat LWE samples as pseudorandom elements.

\end{frame}

\begin{frame}
\frametitle{LWE: grouping samples}
\resizebox{12cm}{!}{
\begin{figure}[H]
    \centering
      \hspace{-30pt}
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/LWE.pdf}
        \caption{Single LWE sample.}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/LWE-n.pdf}
        \caption{$m$ LWE samples.  (Similar to BDD)}
    \end{subfigure}
\end{figure}
}
\end{frame}

\begin{frame}
\frametitle{LWE properties}

\begin{block}{Learning With Errors (LWE)}
  Let $\mathbf{s}$ be a secret vector in $\Zqn$.
  Choose $\mathbf{a} \in \Zqn$ uniformly at random, $e \in \mathbb{Z}$
  according to $\mathcal{X}$ and consider it in $\Zq$, and return
  $\LWE$.
\end{block}

\begin{itemize}
  \item
    If the coefficients of $s$ are taken from the $\mathcal{X}$ error
    distribution, the problem is not easier.
  \item
    Decision-LWE and Search-LWE are equivalent problems under some conditions on
    the parameters.
\end{itemize}
\end{frame}

\section{Ring lattices}

\begin{frame}
\frametitle{Ring lattices definition}
Cryptographic schemes based on LWE have nice properties but are impractical:
\begin{itemize}
  \item Keys are too big: $n \times n$ matrices.
  \item Matrix multiplication is slow: $O(n^2)$.
\end{itemize}
~

We can use a group of lattices called ring lattices that have a special
structure.
\begin{itemize}
  \item
    Lattice bases can be encoded with $n$ coefficients, rather than $n^2$.
  \item
    We work with rings: all the operations are done with polynomials modulo a
    polynomial $f(x)$.
  \item
    This allows better efficiency
    \begin{itemize}
      \item
        Store a lattice with less coefficients: $n$.
      \item
        $O(n \text{ log } n)$ multiplication due to NTT optimization.
    \end{itemize}
\end{itemize}
\end{frame}

\subsection{Ring-LWE}

\begin{frame}
\frametitle{Ring-LWE definition}
We define the ring $R_q = \cycloq$.
\begin{block}{Ring-LWE ($R$-LWE)}
    Let $n$, $q$ be positive integers, $\mathcal{X}$ be a discrete spherical
    probability distribution on $R_q$ called the error distribution and $s$ a
    secret element in $R_q$.  We denote by $\Asx$ the $R$-LWE probability
    distribution on $R_q \times R_q$ obtained by choosing $a \in R_q$ uniformly
    at random, choosing $e \in \mathbb{Z}^n$ according to $\mathcal{X}$ and
    considering it in $R_q$, and returning $\RLWE$.
\end{block}

\end{frame}

\begin{frame}
\frametitle{Ring-LWE definition}
  Like in LWE, two versions of the problem:
\begin{itemize}
  \item
    \alert{Decision-LWE}: deciding whether pairs $(a, c) \in R_q
    \times R_q$ are sampled according to $\Asx$ or the uniform distribution on
    $R_q \times R_q$.

  \item
    \alert{Search-LWE}: recovering $s$ from $\RLWE$ sampled
    according to $\Asx$.
\end{itemize}

Also like in LWE:
\begin{itemize}
  \item
If the coefficients of $s$ are taken from the $\mathcal{X}$ error distribution,
the problem is not easier.
  \item
Decision-R-LWE and Search-R-LWE are equivalent problems under some conditions on the
parameters.
\end{itemize}

\end{frame}

\section{A lattice-based public-key encryption scheme}
\begin{frame}
\frametitle{Ring-LWE encryption}

{\small
\begin{center}
\begin{tabular}{ c c c }
\vspace{-35pt} & & \\
  \includegraphics[width=0.8cm]{figs/alice.png}
  & \framebox[1.2\width]{$a \leftarrow R_q$} \par &
  \includegraphics[width=0.76cm]{figs/bob.png}
\\
& & \\
$s, e \leftarrow \mathcal{X}$
&
$(a, b = a \cdot s + e \text{ } (q))$
\vspace{-7pt}
& \\
&
\vspace{-5pt}
\begin{tikzpicture}
\begin{scope}[thick,->]
  \draw (-2,0)--(2,0);
\end{scope}
\end{tikzpicture}
& \\
&\vspace{10pt} \scriptsize (public key) & \\
&
$u = a \cdot r + e_1 \text{ } (q)$
& $z \in \{0,1\}^n$: msg  \\
&
$v = b \cdot r + e_2 + \qq z \text{ } (q)$
\vspace{-7pt}
& $r, e_1, e_2 \leftarrow \mathcal{X}$\\
&
\vspace{-5pt}
\begin{tikzpicture}
\begin{scope}[thick,->]
  \draw (2,0)--(-2,0);
\end{scope}
\end{tikzpicture}
& \\
            &\scriptsize (ciphertext) & \\
$v - u \cdot s \approx \qq z \text{ } (q)\footnotemark$ & & \\
&
\begin{tikzpicture}
\begin{scope}[dashed, thick]
  \draw (2,0)--(-2,0);
\end{scope}
\end{tikzpicture}
& \\
&
  \includegraphics[width=0.8cm]{figs/eve.png}
& \\
  & $(a, b, u, v)$ & \\
\end{tabular}
\end{center}
}
\footnotetext{\scriptsize $
            v - u \cdot s = (r \cdot e - s \cdot e_1 + e_2) +\
            \qq z \text{ } (q)
          $}
\end{frame}

\begin{frame}
\frametitle{Ring-LWE encryption}
\begin{itemize}
  \item
    Semantically secure.
    \begin{itemize}
      \item
        The attacker only sees $R$-LWE samples, which are pseudorandom due to
        the hardness of decision $R$-LWE.
    \end{itemize}
  \item
    Fast encryption and decryption (ring optimizations).
  \item
    Small keys: $O(n)$.
  \item
    Be careful with the decryption error probability.
\end{itemize}
\end{frame}

\section{Evaluation}

\begin{frame}
  \frametitle{Evaluation}
  \begin{itemize}
    \item We have analyzed the decryption error both theoretically and
      practically to attest that it can be made small for reasonable
      parameters\footnote{Appart from keeping the decryption error small,
      correction codes inside the plaintext could be used.}.
    \item We have proposed a set of parameters for the Ring-LWE encryption
      scheme and evaluated their security.
  \end{itemize}

\begin{table}[H]
    \begin{center}
    \begin{tabular}{c | c | c | c | c | c}
      name & security & $n$ & $q$  & $s$  & P(err) \\ \hline \hline

256\_14' & \approx 128  & 256 & 15361 & 14.7648 & 0.0001\% \\
512\_14' & \approx 256  & 512 & 15361 & 12.4155 & 0.0001\% \\
    \end{tabular}
    \caption{Parameter proposal with estimated error probability per symbol. $s = \sigma / \sqrt{2 \pi}$.}
    \label{param-proposals}
\end{center}
\end{table}
\end{frame}

\begin{frame}
  \frametitle{Evaluation}

  \begin{itemize}
    \item We have implemented the Ring-LWE encryption scheme in C++ and
      benchmarked it with our parameter proposals.
    \item We have studied the current e-voting scheme from Scytl and we have
      proposed the required modifications to make use of the Ring-LWE encryption
      scheme.
  \end{itemize}

\begin{table}[H]
    \begin{center}
    \begin{tabular}{c | c | c | c | c | c | c}
        name & encrypt & decrypt & msg err & sym err & pk size & sk size\\ \hline \hline
256\_14'& 217 $\mu s$ & 126 $\mu s$ & 0.0220\% & 0.0001\% & 7.0 kb & 3.5 kb \\
512\_14'& 450 $\mu s$ & 280 $\mu s$ & 0.0440\% & 0.0001\% & 14.0 kb & 7.0 kb \\
    \end{tabular}
    \caption{Benchmarking results for the Ring-LWE scheme, taken from 50000 runs
    using random plaintexts.}
    \label{bench}
  \end{center}
\end{table}
\end{frame}

\section{Conclusions}

\begin{frame}
  \frametitle{Conclusions}
  \begin{itemize}
    \item We are not doomed, but we need to be ready before quantum computers
      arrive.
    \item Lattice theory contains several hard problems with enough flexibility
      to build cryptosystems based on them.
    \item Ring lattices provide an efficient and competitive encryption scheme.
    \item Adapting a current e-voting scheme to make it post-quantum seems to be
      viable.
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=0.94\paperwidth]{figs/fin.jpeg}
\end{frame}
\end{document}

